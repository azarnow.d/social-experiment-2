﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class PlayerSpawner : NetworkManager {

	public static int chosenCharacter;
	[SerializeField] public GameObject playerA;
	[SerializeField] public GameObject playerB;


	//subclass for sending network messages
	public class NetworkMessage : MessageBase {
		public int chosenClass;
	}

	public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId, NetworkReader extraMessageReader) {
		NetworkMessage message = extraMessageReader.ReadMessage< NetworkMessage>();
//		int selectedClass = message.chosenClass + 1;
//		int selectedClass = message.chosenClass;

		GameObject localPlayer = null;

		GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

//		Debug.Log("server add with message "+ selectedClass);

//		if (selectedClass == 0) {
//			chosenCharacter += 1;
//			GameObject player = Instantiate(playerA) as GameObject;
//			NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
//		}
//
//		if (selectedClass == 1) {
//			GameObject player = Instantiate(playerB) as GameObject;
//			NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
//		}

//		if (Network.connections.Length == 0) {
		if (players.Length == 0) {
			Transform spawn = NetworkManager.singleton.GetStartPosition ();
			localPlayer = Instantiate (playerA, spawn.position, spawn.rotation) as GameObject;
//			playerA = playerB;


		}
		else {
//			if (selectedClass == 1) {
			Transform spawn = NetworkManager.singleton.GetStartPosition ();
			localPlayer = Instantiate (playerB, spawn.position, spawn.rotation) as GameObject;

		} 
		NetworkServer.AddPlayerForConnection (conn, localPlayer, playerControllerId);
	}

	public override void OnClientConnect(NetworkConnection conn) {
		NetworkMessage test = new NetworkMessage();
		test.chosenClass = chosenCharacter;
		chosenCharacter += 1;
		ClientScene.AddPlayer(conn, 0, test);
	}


	public override void OnClientSceneChanged(NetworkConnection conn) {
		//base.OnClientSceneChanged(conn);
	}
}
