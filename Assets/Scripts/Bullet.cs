﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class Bullet : NetworkBehaviour
{
	public float moveSpeed = 10.0f;
	public Vector3 velocity;
	Renderer[] renderers;

	private void FixedUpdate()
	{
		// we want the bullet to be updated only on the server
//		if (!base.isServer)
//			return;

		// transform bullet on the server
		transform.position += velocity * Time.deltaTime * moveSpeed;
	}

	// void OnTriggerEnter(Collider other) {
	// 	if (other.gameObject.CompareTag("Player")) {
	// 		renderers = other.GetComponentsInChildren<Renderer> ();
	// 		StartCoroutine (Dead ());

	// 	}
	// }

	// IEnumerator Dead(){
	// 	foreach (Renderer r in renderers)
	// 		r.enabled = false;
	// 	//Destroy(this.gameObject);
	// 	yield return new WaitForSeconds (1);
	// 	foreach (Renderer r in renderers)
	// 		r.enabled = true;
	// }
}




