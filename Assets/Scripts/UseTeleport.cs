﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;
using System;

public class UseTeleport : NetworkBehaviour {
	
	private static GameObject thisTeleport;
	private static GameObject otherTeleport;
	public GameObject greetButton;
	public GameObject kickButton;
	public GameObject activateButton;
	public GameObject takeOffButton;
	public GameObject saveButton;
	public GameObject escapeButton;
	public GameObject canvas;
	public GameObject pos_1;
	public GameObject pos_2;


//	Instance activation;
	private static String active = "not active";
	private static String isTeleported = "not teleported"; 
	public static String saveOther = "not saved";
	[SyncVar]
	private GameObject objectID;

//	public bool stay;

	private NetworkIdentity objNetId;

	// Update is called once per frame

	void Start(){
		Transform thisCanvas = (Instantiate (canvas)).transform;
		greetButton =  thisCanvas.GetChild (0).gameObject; 
		kickButton =  thisCanvas.GetChild (1).gameObject; 
		activateButton = thisCanvas.GetChild (2).gameObject; 
		takeOffButton = thisCanvas.GetChild (3).gameObject; 
		saveButton = thisCanvas.GetChild (4).gameObject; 
		escapeButton = thisCanvas.GetChild (5).gameObject; 

//		kickButton = GameObject.FindGameObjectWithTag ("Kick");
//		activateButton = GameObject.FindGameObjectWithTag ("Activate");
//		takeOffButton = GameObject.FindGameObjectWithTag ("Take Off");
//		saveButton = GameObject.FindGameObjectWithTag ("Save");
//		escapeButton = GameObject.FindGameObjectWithTag ("Escape");

		activateButton.SetActive (false);
		takeOffButton.SetActive (false);
		saveButton.SetActive (false);
		escapeButton.SetActive (false);
		Debug.Log("BUTTONS " + activateButton.gameObject.tag + " / " + takeOffButton.gameObject.tag);

	}

	void Update () {
		
		if (isLocalPlayer) {

			if (isTeleported == null) {
//				this.gameObject.GetComponent<NavMeshAgent> ().enabled = false;

				Cmd_TransformPlayer (this.transform.gameObject, pos_1.transform.position);
//				this.gameObject.GetComponent<NavMeshAgent>().ResetPath();
//				this.transform.position = pos_1.transform.position;
				greetButton.SetActive (false);
				kickButton.SetActive (false);
				saveButton.SetActive (true);
				escapeButton.SetActive (true);
				if (saveOther == null) {
					if (Input.GetMouseButton (0)) {
						RaycastHit hit;
						Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);

						if (Physics.Raycast (ray, out hit)) {
							Debug.Log ("Physics Raycast");
							if (hit.transform.CompareTag ("Player")) {
								Debug.Log ("inside Update PUSH");
//								hit.transform.gameObject.GetComponent<NavMeshAgent> ().enabled = false;

								Cmd_TransformPlayer (hit.transform.gameObject, pos_2.transform.position);
//								hit.transform.gameObject.GetComponent<NavMeshAgent>().ResetPath();
//								hit.transform.gameObject.GetComponent<Transform> ().position = pos_2.transform.position;
//								hit.transform.gameObject.GetComponent<UseTeleport> ().ActivateTakeOffButton ();

							}
						}
					}
				}
			}

				

			Debug.Log ("INSIDE UPDATE LOCAL PLAYER");
		

			if (active == null) {
				active = "not active";
				objectID = otherTeleport;

				Debug.Log ("INSIDE UPDATE ACTIVATE OTHER = " + objectID);
				CmdActivateOtherTeleport (objectID);
			}
		}

	}

	[Command]
	void CmdActivateOtherTeleport (GameObject obj){
		objNetId = obj.GetComponent<NetworkIdentity> ();
		objNetId.AssignClientAuthority (connectionToClient);
		Rpc_ActivateOtherTeleport (obj);
		objNetId.RemoveClientAuthority (connectionToClient);
		
//		Debug.Log ("INSIDE CMD ACTIVATE OTHERTELEPORT = " + otherTeleport);
//		Debug.Log("ACTIVE CMD" + active);
//		thisTeleport.GetComponent<Teleportation> ().ActivateTeleport ();
//		active = "not active";
//		Debug.Log ("Finish");

	}

	[ClientRpc]
	void Rpc_ActivateOtherTeleport(GameObject obj){
		Debug.Log("INSIDE CLIENTRPC");
//		obj.GetComponent<Teleportation> ().ActivateTeleport ();
		ParticleSystem tp = obj.GetComponent<ParticleSystem> ();
		if (tp.isPlaying) {
			tp.Stop ();

		} else if (tp.isStopped) {
			tp.Play ();
		}
	}


	[Command]
	void CmdStopOtherTeleport (GameObject obj){
		objNetId = obj.GetComponent<NetworkIdentity> ();
		objNetId.AssignClientAuthority (connectionToClient);
		Rpc_StopOtherTeleport (obj);
		objNetId.RemoveClientAuthority (connectionToClient);

		//		Debug.Log ("INSIDE CMD ACTIVATE OTHERTELEPORT = " + otherTeleport);
		//		Debug.Log("ACTIVE CMD" + active);
		//		thisTeleport.GetComponent<Teleportation> ().ActivateTeleport ();
		//		active = "not active";
		//		Debug.Log ("Finish");

	}

	[ClientRpc]
	void Rpc_StopOtherTeleport(GameObject obj){
		Debug.Log("INSIDE CLIENTRPC");
		//		obj.GetComponent<Teleportation> ().ActivateTeleport ();
		ParticleSystem tp = obj.GetComponent<ParticleSystem> ();
			tp.Stop ();
	}

	[Command]
	void Cmd_TransformPlayer(GameObject obj, Vector3 position){
		Rpc_TransformPlayer (obj, position);

		
	}

	[ClientRpc]
	void Rpc_TransformPlayer(GameObject obj, Vector3 position){
//		obj.GetComponent<Transform> ().position = position;
		obj.GetComponent<NavMeshAgent>().Warp(position);
	}

	void OnTriggerEnter(Collider other)
	{
		Debug.Log("Entering " + thisTeleport);
		if(isLocalPlayer)
		if(other.gameObject.tag == "Platform_1" || other.gameObject.tag == "Platform_2"){
			Debug.Log ("OnTriggerEnter");
			activateButton.SetActive(true);
		}

		if (other.gameObject.tag == "Platform_1") {
			thisTeleport = other.gameObject;
			otherTeleport = GameObject.FindGameObjectWithTag ("Platform_2");
						Debug.Log ("ON ENTER P1 " + otherTeleport);
		
		} else if (other.gameObject.tag == "Platform_2") {

			thisTeleport = other.gameObject;
			otherTeleport = GameObject.FindGameObjectWithTag ("Platform_1");
			Debug.Log ("ON ENTER P2 " + otherTeleport);

		}
	}


	void OnTriggerStay(Collider other)
	{
		if(isLocalPlayer)
		if (other.gameObject.tag == "Platform_1"){
			thisTeleport = other.gameObject;
			otherTeleport = GameObject.FindGameObjectWithTag ("Platform_2");
			Debug.Log ("ON Stay P1" + otherTeleport);
			if (thisTeleport.GetComponent<ParticleSystem> ().isPlaying) {
				takeOffButton.SetActive (true);
			} else if (thisTeleport.GetComponent<ParticleSystem> ().isStopped) {
				takeOffButton.SetActive (false);
			}

		}	else if( other.gameObject.tag == "Platform_2"){

			thisTeleport = other.gameObject;
			otherTeleport = GameObject.FindGameObjectWithTag ("Platform_1");
			Debug.Log ("ON Stay P2" + otherTeleport);

			if (thisTeleport.GetComponent<ParticleSystem> ().isPlaying) {
				takeOffButton.SetActive (true);
			} else if (thisTeleport.GetComponent<ParticleSystem> ().isStopped) {
				takeOffButton.SetActive (false);
			}
//			Debug.Log ("ON ENTER P2");
			//			Debug.Log ("OnTriggerEnter");
			//			//			otherTeleport.SetActive(false);
			//			//			thisTeleport.SetActive(false);
			//			activateButton.SetActive(true);
		}
	}

	void OnTriggerExit(Collider other)
	{
		if(isLocalPlayer)
		if (other.gameObject.tag == "Platform_1" || other.gameObject.tag == "Platform_2") {
			takeOffButton.SetActive (false);
			activateButton.SetActive (false);

			Debug.Log ("EXIT");

			CmdStopOtherTeleport (otherTeleport);

//			thisTeleport.GetComponent<ParticleSystem> ().Stop ();
//			otherTeleport.GetComponent<ParticleSystem> ().Stop ();
			thisTeleport = null;
			otherTeleport = null;
		}
	}

	public void ActivateTeleportOnClick(){
//				if (!isLocalPlayer) {
//					return;
		Debug.Log ("INSIDE  ACTIVATE TELEPORT ONCLICK");
		active = null;
		Debug.Log ("activateOther " + active);
		}

	public void ActivateTeleport(){
		active = null;
	}

	public void ActivateTakeOffButton(){
//		this.transform.localPosition = new Vector3(-11.77f,-3.0f,2.44f);

//		this.GetComponent<Rigidbody>().MovePosition(new Vector3(-11.77f,-3.0f,2.44f));
		isTeleported = null;
		Debug.Log ("TAKE OFF" +  this.gameObject.name);
//		this.gameObject.transform.rotation = 
	}

	public void ActivateSaveButton(){
		saveOther = null;
		Debug.Log ("Inside save" + saveOther);
	}

}
	

//	public override void OnStartLocalPlayer(){
//		localPlayer = this;
//	}


//	public void ActivateTeleport(){
//		if (!isLocalPlayer) {
//			return;
//		}
//		Debug.Log ("INSIDE ACTIVATE TELEPORT");
//		Collider[] hitColliders = Physics.OverlapSphere(this.transform.position, .99f);
//		Debug.Log (hitColliders.Length);
//		Debug.Log (hitColliders[0]);
//		Debug.Log (hitColliders[1]);
//		int i = 0;
//		while (i < hitColliders.Length) {
//			Debug.Log("HIT COLLIDERS  " + hitColliders[i]);
//			if ((hitColliders [i].tag == "Platform_1" || hitColliders [i].name == "Platform_2")
//			   && hitColliders [i].gameObject != gameObject) {
//				Debug.Log ("SELECTED COLLIDER" + hitColliders[i]);
//				thisTeleport = hitColliders [i].gameObject;
//				Debug.Log ("THIS TELEPORT " + thisTeleport);
//				otherTeleport = thisTeleport.gameObject
//					.GetComponent<Teleportation> ()
//					.otherTeleport;
//				Debug.Log ("OTHER TELEPORT " + otherTeleport);
//				break;
//			}
//			i++;
//		}

	
//		Debug.Log ("isServer " + this.GetComponent<NetworkIdentity>().isServer);
//		Debug.Log ("isLocalPlayer " + isLocalPlayer);
//		Debug.Log (this.gameObject);
//
//		if (isLocalPlayer) {
//			Debug.Log ("INSIDEisLocalPlayer");
//			activated = !activated;
//			Debug.Log (activated);
//			CheckIfOtherIsActive ();
//		}
//	}
		
	//	void Update(){
	////		if(isLocalPlayer)
	////		yield WaitForSecondsRealtime(1.0f);
	//
	//	}

//	void CheckIfOtherIsActive (){
//				//		if (stay) {
//		objectID = otherTeleport;
//
//		CmdActivateOther (objectID,activated);
//		//		}
//
//	}
//
//	[Command]
//	void CmdActivateOther(GameObject obj, bool active){
//		Debug.Log ("Inside CMD " + active);
//		objNetId = obj.gameObject.GetComponent<NetworkIdentity> ();
//		Debug.Log ("Inside CMD objNetId = " + objNetId);
//		objNetId.AssignClientAuthority (objNetId.connectionToClient);
//		Rpc_ActivateOtherOnClients (obj, active);
//		objNetId.RemoveClientAuthority (objNetId.connectionToClient);
//	}
//
//	[ClientRpc]
//	void Rpc_ActivateOtherOnClients(GameObject obj, bool active){
//		Debug.Log ("Inside RPC " + active);
//		if (active) {
//			obj.GetComponent<ParticleSystem> ().Play ();
//		} else {
//			obj.GetComponent<ParticleSystem> ().Stop ();
//		}
//	}
//}
