﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;


public class Teleportation : NetworkBehaviour {

	public GameObject thisTeleport; 
	public GameObject otherTeleport;
	public GameObject activateButton;
	public GameObject takeOffButton;
	
//	[SyncVar]
//	bool activated;
//	[SyncVar]
//	private GameObject objectID;
//	public bool stay;
//	private NetworkIdentity objNetId;




	// Use this for initialization
	void Start () {
		Debug.Log (otherTeleport);

//		thisTeleport.GetComponent<ParticleSystem>().Stop ();
//		otherTeleport.GetComponent<ParticleSystem>().Stop ();
		//NetworkServer.Spawn (this.gameObject);
		//NetworkClient networkClient = new NetworkClient();
		//clientConnection = networkClient.connection;
		// otherTeleportTrigger = otherTeleport.GetComponent<BoxCollider>();
		// thisTeleportArea = this.GetComponent<BoxCollider>();
		// Button activation = activate.GetComponent<Button>();	
		// activate.GetComponent<Button>().OnClick.AddListener(ActivateOtherTeleport);
	}


//	public void ActivateTeleport(){
//		
//		activated = !activated;
//		Debug.Log ("Activate teleportation activated " + activated);
//		Debug.Log (activated);
////		CheckIfOtherIsActive ();
//	}

//	void Update(){
////		if(isLocalPlayer)
////		yield WaitForSecondsRealtime(1.0f);
//
//	}

//	void CheckIfOtherIsActive (){
//	
////		if (stay) {
//			objectID = otherTeleport;
//
//			CmdActivateOther (objectID,activated);
////		}
//
//	}

//	[Command]
//	void CmdActivateOther(GameObject obj, bool active){
//		Debug.Log ("Inside CMD " + active);
//		objNetId = obj.gameObject.GetComponent<NetworkIdentity> ();
//		Debug.Log ("Inside CMD objNetId = " + objNetId);
//		objNetId.AssignClientAuthority (objNetId.connectionToClient);
//		Rpc_ActivateOtherOnClients (obj, active);
//		objNetId.RemoveClientAuthority (objNetId.connectionToClient);
//	}
//
//	[ClientRpc]
//	void Rpc_ActivateOtherOnClients(GameObject obj, bool active){
//		Debug.Log ("Inside RPC " + active);
//		if (active) {
//			obj.GetComponent<ParticleSystem> ().Play ();
//		} else {
//			obj.GetComponent<ParticleSystem> ().Stop ();
//		}
//	}




	
	// public void TakeOffAction(){
		
	// 	vfx.SetActive(true);
	// 	Instantiate(vfx, thisTeleport.transform.position, thisTeleport.transform.rotation);
	// 	vfx.SetActive(false);
	// }

    /// <summary>
	/// OnTriggerEnter is called when the Collider other enters the trigger.
	/// </summary>
	/// <param name="other">The other Collider involved in this collision.</param>
	void OnTriggerEnter(Collider other)
	{
		Debug.Log("Entering " + thisTeleport);
		if(other.gameObject.tag == "Player"){
			Debug.Log ("OnTriggerEnter");
			activateButton.SetActive(true);
		}
	}

	/// <summary>
	/// OnTriggerStay is called once per frame for every Collider other
	/// that is touching the trigger.
	/// </summary>
	/// <param name="other">The other Collider involved in this collision.</param>
	void OnTriggerStay(Collider other)
	{
		if (other.gameObject.tag == "Player") {
			if (thisTeleport.GetComponent<ParticleSystem> ().isPlaying) {
				takeOffButton.SetActive (true);
			} else {
				takeOffButton.SetActive (false);
			}
		}
	} 


	 /// <summary>
	/// OnTriggerExit is called when the Collider other has stopped touching the trigger.
	/// </summary>
	/// <param name="other">The other Collider involved in this collision.</param>
	void OnTriggerExit(Collider other)
	{
		Debug.Log ("OnTriggerExit");
		Debug.Log("Exiting " + thisTeleport);
		if(other.gameObject.tag == "Player" ){
//			stay = false;
//			thisTeleport.SetActive(false);
//			otherTeleport.SetActive(false);
			activateButton.SetActive(false);
//			otherTeleport.GetComponent<ParticleSystem> ().Stop ();
			takeOffButton.SetActive(false);
//			activated = false;
			}
	}
	
}