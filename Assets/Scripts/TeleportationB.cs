﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class TeleportationB : MonoBehaviour {

	public GameObject otherTeleport; 
	public GameObject activateButton;
	public GameObject takeOffButton;
	// public GameObject otherTeleportTrigger;
	public GameObject thisTeleport;
	Collider thisTeleportArea;
	bool activated = false;
	public Button activate;


	// Use this for initialization
	void Start () {
		// otherTeleportTrigger = otherTeleport.GetComponent<BoxCollider>();
		// thisTeleportArea = this.GetComponent<BoxCollider>();
		// Button activation = activate.GetComponent<Button>();	
		// activate.GetComponent<Button>().OnClick.AddListener(ActivateOtherTeleport);
	}

	void Update(){
			
	}

	// public void ActivateOtherTeleport(){
	// 	activated = !activated;
	// 	Debug.Log("ActivateOtherTeleport");
	// }

	 /// <summary>
	/// OnTriggerEnter is called when the Collider other enters the trigger.
	/// </summary>
	/// <param name="other">The other Collider involved in this collision.</param>
	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag == "Player"){
			otherTeleport.SetActive(activated);
			activateButton.SetActive(true);
			

			if(thisTeleport.activeSelf){
			takeOffButton.SetActive(true);
		}
			
		}
	}

	 /// <summary>
	/// OnTriggerStay is called once per frame for every Collider other
	/// that is touching the trigger.
	/// </summary>
	/// <param name="other">The other Collider involved in this collision.</param>
	// void OnTriggerStay(Collider other)
	// {
	// 	if(other.gameObject.tag == "Player"){
	// 		// if(activated)
	// 		otherTeleport.SetActive(true);
	// 		Debug.Log("otherTeleportActive");
	// 	}
	// }

	 /// <summary>
	/// OnTriggerExit is called when the Collider other has stopped touching the trigger.
	/// </summary>
	/// <param name="other">The other Collider involved in this collision.</param>
	void OnTriggerExit(Collider other)
	{
		if(other.gameObject.tag == "Player"){
			otherTeleport.SetActive(false);
			activateButton.SetActive(false);
			takeOffButton.SetActive(false);
		}
	}
}
