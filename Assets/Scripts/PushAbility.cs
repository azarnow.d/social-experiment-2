﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;

public class PushAbility : NetworkBehaviour {


	//public Texture2D ab1;
	//public Texture2D ab1CD;
//	float pushTimer = 0;
	public float pushCDTime;
//	bool pushed = true;
	private static string pushed = "not pushed";
//	bool pushedCD = true;
	public float moveSpeed = 1000000;
	public float rotationSpeed;
	private Vector3 targetDir;
	public GameObject button;
	private Button push;
	public GameObject pushCube;
	public Transform bulletSpawn;
//	public Transform VFX;


//	NavMeshAgent agent;


	void Start(){


		// agent = GetComponent<NavMeshAgent> ();
		ClientScene.RegisterPrefab (pushCube);

		
		//push = GameObject.Find("Kick").GetComponent<Button>();
//		push = button.GetComponent<Button>();

	}


	public void ClickToPush(){
		Debug.Log ("IN PUSH Click");
		pushed = "";
		Debug.Log (pushed);
	}

	// void OnGUI(){

	// 	pushTimer -= Time.deltaTime;
	// //	bool pushKey = Input.GetKeyDown (KeyCode.R);

	// 	if (pushTimer <= 0) {
	// 		if (GUI.Button(new Rect(1000, 450, 340,125), "Push")) {
	// 		pushed = false;
	// 	}

	// 		// if (pushKey) {
	// 		// 	Push ();
	// 		// }

	// 	} else {
	// 		GUI.Button(new Rect(1000, 450, 340,125), "PushCD");
	// 	}
	// }

	// void Push(){
	// 	pushTimer = pushCDTime;
	// }

	void Update(){
		// bool pushKey = Input.GetKeyDown (KeyCode.R);

		// if (pushKey) {
		// 	pushed = false;
		// }
//		push.onClick.AddListener(PushPlayer);
		if (!isLocalPlayer) {
			return;
		}
//
		Transform player;


		if (Input.GetMouseButton (0) && pushed == "") {
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);

			if (Physics.Raycast (ray, out hit)) {
				Debug.Log("Physics Raycast");
				if (hit.transform.CompareTag ("Player")) {
					Debug.Log("inside Update PUSH");
					player = hit.transform;
					targetDir = player.position - this.transform.position;
					float step = rotationSpeed * Time.deltaTime;
			
					Vector3 newDir = Vector3.RotateTowards (transform.forward, targetDir, step, 0.0F);
					Debug.DrawRay (transform.position, newDir, Color.red);
					transform.rotation = Quaternion.LookRotation (newDir);
					// agent.speed = 100;
					
					
					
					// agent.SetDestination (player.transform.position);
					Cmd_CollidersIdentify(this.transform.position, 5.0f);
					//agent.Move(targetDir);


					


					//this.transform.position += this.transform.forward * moveSpeed * Time.deltaTime;
					//if (shootKey) {
					
					//}
				}
			}
		}

	}

	public void AI_AttackPlayer(GameObject ai_target){
		
			Transform player = ai_target.transform;
			targetDir = player.position - this.transform.position;
			float step = rotationSpeed * Time.deltaTime;

			Vector3 newDir = Vector3.RotateTowards (transform.forward, targetDir, step, 0.0F);
			Debug.DrawRay (transform.position, newDir, Color.red);
			transform.rotation = Quaternion.LookRotation (newDir);
			// agent.speed = 100;



			// agent.SetDestination (player.transform.position);
			Cmd_CollidersIdentify(this.transform.position, 5.0f);

	}

//	void PushPlayer(){
//		pushed = false;
//	}

	[Command]
		 void Cmd_CollidersIdentify(Vector3 center, float radius) {
         Collider[] hitColliders = Physics.OverlapSphere(center, radius);
         int i = 0;
         while (i < hitColliders.Length) {
             if(hitColliders[i].tag =="Player" && hitColliders[i].gameObject != gameObject)
                 {
					 Debug.Log("Push");

//					 GameObject obj = (GameObject)Instantiate (pushCube, bulletSpawn.position, bulletSpawn.rotation);
//					 Instantiate(VFX,  bulletSpawn.position, bulletSpawn.rotation);
//					 hitColliders[i].GetComponent<NavMeshAgent>().ResetPath();
//				GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
//				foreach (GameObject player in players) {
//					player.GetComponent<NavMeshAgent> ().ResetPath ();
//				}
						// setup bullet component
//					Bullet bullet = obj.GetComponent<Bullet> ();
//					bullet.velocity = transform.forward;
				GameObject player = hitColliders[i].gameObject;
				Rpc_Move (player);
//				obj.GetComponent<Rigidbody>().velocity = transform.forward*400;
						// destroy after 2 secs

						// spawn on the clients
//				NetworkServer.Spawn (obj);

//				Destroy (obj, 2.0f);

					//  hitColliders[i].GetComponent<Rigidbody>().velocity = (targetDir * 10);
                    // hitColliders[i].GetComponent<Rigidbody>().AddForce(targetDir * 10000);
				//	hitColliders[i].GetComponent<Rigidbody>().MovePosition(targetDir * Time.fixedDeltaTime );
				//    hitColliders[i].transform.position += Vector3.forward * Time.deltaTime * moveSpeed;
				//    agent.ResetPath();
				//    agent.speed = 5;
				   pushed = "not pushed";
                 }
             i++;
             }
         }

	[ClientRpc]
	void Rpc_Move(GameObject player){
		player.GetComponent<NavMeshAgent>().velocity = transform.forward*15;
		player.GetComponent<NavMeshAgent>().ResetPath();
	}
     

	// void OnCollisionEnter(Collision collision){
	// 	if (collision.gameObject.CompareTag ("Player")) {
	// 		collision.gameObject.GetComponent<Rigidbody> ().AddForce (Vector3.forward * 100);
	// 		NavMeshAgent playerAgent = this.gameObject.GetComponent<NavMeshAgent>();
	// 		//playerAgent.stoppingDistance = 1;
	// 		Debug.Log("Collision");

	// 			playerAgent.speed = 5;
	// 		// if(!playerAgent.pathPending){
	// 		// 	if (playerAgent.remainingDistance <= playerAgent.stoppingDistance) {
	// 		// 		if (!playerAgent.hasPath || playerAgent.velocity.sqrMagnitude == 0f) {
	// 		// 		
	// 		// 		}
	// 		// 	}
				
	// 		// }
				

	// 	}
	// }
	/*
	[Command]
	void Cmd_Shoot(){

		GameObject obj = (GameObject)Instantiate (bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);
		// setup bullet component
		Bullet bullet = obj.GetComponent<Bullet> ();
		bullet.velocity = transform.forward;
		// destroy after 2 secs
		Destroy (obj, 2.0f);
		// spawn on the clients
		NetworkServer.Spawn (obj);
		shootTimer = shootCDTime;
		fired = true;
		pushedCD = false;
		StartCoroutine (WaitCD());

	}

	IEnumerator WaitCD(){
		yield return new WaitForSeconds(shootCDTime);
		pushedCD = true;

	}
 */
}
