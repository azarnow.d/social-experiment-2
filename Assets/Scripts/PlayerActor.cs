﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;

public class PlayerActor : NetworkBehaviour
{

    [SyncVar]
    public float speed;
    public float rotationSpeed;
	// public GameObject bulletPrefab = null;
	// public Transform bulletSpawn = null;
	NavMeshAgent agent;
	private AudioSource helloSound;
	public static string sayHello = null;
//    private Rigidbody rb;
//	[SyncVar]
//	Queue queue = new Queue();


    void Start()
    {
		
//        rb = GetComponent<Rigidbody>();
		// ClientScene.RegisterPrefab (bulletPrefab);
		agent = GetComponent<NavMeshAgent>();
		helloSound = GetComponent<AudioSource> ();

    }

    void FixedUpdate()
    {
		if (!isLocalPlayer) {
			return;
		}
		// float translation = CrossPlatformInputManager.GetAxis("Vertical") * speed;
		//  float rotation = CrossPlatformInputManager.GetAxis("Horizontal") * rotationSpeed;

		// translation *= Time.deltaTime;
		// rotation *= Time.deltaTime;
       // transform.Translate(0, 0, translation);
       // transform.Rotate(0, rotation, 0);
        
		if (Input.GetMouseButtonDown(0)) {
			RaycastHit hit;

			if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100)) {
				if (hit.transform.CompareTag ("Ground") || hit.transform.CompareTag ("Platform_1")
				    || hit.transform.CompareTag ("Platform_2")) {
					agent.destination = hit.point;
				} if(sayHello != null && hit.transform.CompareTag ("Player")){
					
					Vector3 newDir = Vector3.RotateTowards (transform.forward, hit.transform.position - this.transform.position
						, 1000 * Time.deltaTime, 0.0F);
					transform.rotation = Quaternion.LookRotation (newDir);

					helloSound.Play ();
					sayHello = null;
				}
			}
		}
    }

	public void OnGreetHello(){
		sayHello = "sayHello";
	}

	public void AI_GreetPlayer(GameObject ai_target){

		Transform player = ai_target.transform;
		Vector3 targetDir = player.position - this.transform.position;
		float step = rotationSpeed * Time.deltaTime;

		Vector3 newDir = Vector3.RotateTowards (transform.forward, targetDir, step, 0.0F);
		Debug.DrawRay (transform.position, newDir, Color.red);
		transform.rotation = Quaternion.LookRotation (newDir);
		// agent.speed = 100;
		helloSound.Play ();
		sayHello = null;

	}

	// [Command]
	// public void Cmd_Shoot()
	// {
	// 	// create server-side instance
	// 	GameObject obj = (GameObject)Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);
	// 	// setup bullet component
	// 	Bullet bullet = obj.GetComponent<Bullet>();
	// 	bullet.velocity = transform.forward;
	// 	// destroy after 2 secs
	// 	Destroy(obj, 2.0f);
	// 	// spawn on the clients
	// 	NetworkServer.Spawn(obj);
	// }




}